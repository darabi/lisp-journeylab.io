+++
date = "2017-02-05T07:51:49+01:00"
title = "Home"
draft = false

+++

My **Common Lisp journey**. Discovering the language and the ecosystem.

Will post short articles about libraries, code snippets, other
resources, tips and tricks, opinions. Much from reddit, more from my
exploration. Watch if they'd better go to the
[Cookbook](https://github.com/LispCookbook/cl-cookbook).

Because Common Lisp is the most hidden world I know. It's puzzling and
a shame, really.

I have some experience with Lisp writting [Emacs Lisp packages](https://gitlab.com/emacs-stuff/), but I
mostly write Python (and Javascript) at work and for my personal
projects. I'm frustrated by its limitations and I see great potential
in CL for web apps, specially with the [awesome update of Weblocks going on](https://github.com/40ants/weblocks/blob/reblocks/docs/source/quickstart.rst), so this will be the direction of my wanderings.

---

Discuss (and share tips ?) on
https://gitlab.com/lisp-journey/lisp-journey.gitlab.io/issues
