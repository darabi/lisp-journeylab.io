+++
date = "2017-02-05T07:51:49+01:00"
title = "Resources"
draft = false
+++


* [lisp-lang.org](http://lisp-lang.org/)
* [awesome-cl](https://github.com/CodyReichert/awesome-cl)
* [Common Lisp Cookbook](https://lispcookbook.github.io/cl-cookbook/)

and

* http://quickdocs.org/
* http://articulate-lisp.com

Individual sites:

* [sjl's road to Lisp](http://stevelosh.com/blog/2018/08/a-road-to-common-lisp/)
* https://www.darkchestnut.com/ - he encountered deployment obstacles, wrote a book and blogs about it.
* http://lispmethods.com/
* [Malisper's series on debugging](http://malisper.me//debugging-lisp-part-1-recompilation/)

Screencasts:

* [Little Bits of Lisp](https://www.youtube.com/playlist?list=PL2VAYZE_4wRJi_vgpjsH75kMhN4KsuzR_) -
  short videos on various topics: inspecting a condition, basics of
  lisp's evaluation model,…
* [Common Lisp Tutorials](https://www.youtube.com/playlist?list=PL2VAYZE_4wRIoHsU5cEBIxCYcbHzy4Ypj), of which [Emacs and Slime - useful keyboard shortcuts](https://www.youtube.com/watch?v=sBcPNr1CKKw&index=4&list=PL2VAYZE_4wRIoHsU5cEBIxCYcbHzy4Ypj)
* [Common Lisp Study Group](https://www.youtube.com/channel/UCYg6qFXDE5SGT_YXhuJPU0A/videos) (long videos)
* [Web development in Emacs with Common Lisp and ClojureSCript](https://www.youtube.com/watch?v=bl8jQ2wRh6k) -
  building [Potato](https://github.com/cicakhq/potato), a Slack-like app.
* Shinmera playlists:
  [Treehouse](https://www.youtube.com/playlist?list=PLkDl6Irujx9MtJPRRP5KBH40SGCenztPW)
  (game dev),
  [Demos](https://www.youtube.com/playlist?list=PLkDl6Irujx9Mh3BWdBmt4JtIrwYgihTWp)
  (of small programs).
* [Pushing pixels with Lisp](https://www.youtube.com/watch?v=82o5NeyZtvw), and by the same author:
 * [CEPL demo](https://www.youtube.com/watch?v=a2tTpjGOhjw&index=20&list=RDxzTH_ZqaFKI) - working with OpenGL
 * the [author's channel](https://www.youtube.com/channel/UCMV8p6Lb-bd6UZtTc_QD4zA).

* [McClim interactive GUI demos](https://www.youtube.com/watch?v=XGmo0E_S46I). [Code examples](https://github.com/robert-strandh/McCLIM/blob/master/Examples/demodemo.lisp). Presentation of [Clim listener, Clim debugger, drawing objects into the GUI repl](https://www.youtube.com/watch?v=kfBmRsPRdGg).

and more on [Cliki](http://www.cliki.net/Lisp%20Videos).

Example websites built with Lisp:

* [Quickdocs-server](https://github.com/quickdocs/quickdocs-server) - Caveman, Djula templates, Datafly and Sxql, Envy configuration switcher, Qlot, a simple fabfile for deployment.
* [Quickutil](https://github.com/tarballs-are-good/quickutil/blob/master/quickutil-server/) -
  ningle, closure-template, jquery and pjax, hot deploy with
  connection to a swank server, a fabfile, nginx, supervisor, watchdog
  autoreload.

See also Potato, Turtl and others in the Software section.
